package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CanvasController implements Initializable
{
    /*
     * checkbox example
     */
    @FXML private Label pizzaOrderLabel;
    @FXML private CheckBox pepperoniCheckBox;
    @FXML private CheckBox pineappleCheckBox;
    @FXML private CheckBox baconCheckBox;
    /*
     * Choicebox example
     */
    @FXML private ChoiceBox<String> choiceBox;
    @FXML private Label choiceBoxLabel;
    /*
     * ComboBox example
     */
    @FXML private ComboBox<String> comboBox;
    @FXML private Label comboBoxLabel;
    /*
     * RadioButton example
     */
    @FXML private RadioButton phpRadioButton;
    @FXML private RadioButton Java;
    @FXML private RadioButton cSharp;
    @FXML private RadioButton cPlusPlus;
    @FXML private Label radioLabel;
    private ToggleGroup favLangToggleGroup;
    /*
     * Listview and textArea example
     */
    @FXML private ListView<String> listView;
    @FXML private TextArea golfTextArea;
    /*
     * Spinner example
     */
    @FXML private Spinner<Integer> gradeSpinner;
    @FXML private Button getGradeButton;
    @FXML private Label gradeLabel;

    /*
     * controller for the choicebox
     */
    public void choiceboxButtonPushed()
    {
        choiceBoxLabel.setText("Favourite fruit is:\n"+choiceBox.getValue().toString());
    }
    /*
     * controller for the checkbox
     */
    public void pizzaorderButtonPushed()
    {
        String order = "Toppings are:";

        if(pineappleCheckBox.isSelected())
        {
            order +="\npineapple";
        }
        if(pepperoniCheckBox.isSelected())
        {
            order +="\npepperoni";
        }
        if(baconCheckBox.isSelected())
        {
            order +="\nbacon";
        }

        pizzaOrderLabel.setText(order);
    }
    /*
     * controller for the comboBox
     */
    public void updateComboBox()
    {
        comboBoxLabel.setText("Course selected: \n" + comboBox.getValue());
    }
    /*
     * controller for radioButton
     */
    public void radioButtonChanged()
    {
        if(favLangToggleGroup.getSelectedToggle().equals(cPlusPlus)) {
            radioLabel.setText("The selected item is: \nC++");
        }
        if(favLangToggleGroup.getSelectedToggle().equals(Java)) {
            radioLabel.setText("The selected item is: \nJava");
        }
        if(favLangToggleGroup.getSelectedToggle().equals(cSharp)) {
            radioLabel.setText("The selected item is: \nC#");
        }
        if(favLangToggleGroup.getSelectedToggle().equals(phpRadioButton)) {
            radioLabel.setText("The selected item is: \nPHP");
        }
    }
    /*
     * controller for listview to textarea
     */
    public void listViewButtonPushed()
    {
        String textAreaString = "";
        ObservableList<String> listOfItems = listView.getSelectionModel().getSelectedItems();

        for(Object item :listOfItems){
            textAreaString += String.format("%s%n", item);
        }

        golfTextArea.setText(textAreaString);

    }

    /*
     * change the scene to tableview
     */
    public void changeScreenButtonPushedToTableView(ActionEvent event) throws IOException
    {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("TableView.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();

//		Stage newWindow = new Stage();
//		newWindow.setScene(tableViewScene);
//		newWindow.show();

    }

    /*
     * controller spinner
     */
    public void getGradeButtonPushed()
    {
        gradeLabel.setVisible(true);
        gradeLabel.setText(gradeSpinner.getValue().toString());
    }


    @Override
    public void initialize(URL arg0, ResourceBundle arg1)
    {
        pizzaOrderLabel.setText("");

        /*
         * configuring choicebox
         */
        choiceBoxLabel.setText("");
        choiceBox.getItems().add("apples");
        choiceBox.getItems().add("bananas");
        choiceBox.getItems().addAll("oranges","pears","kiwis","Choose");
        choiceBox.setValue("Choose");
        /*
         * configuring Combobox
         */
        comboBoxLabel.setText("");
        comboBox.getItems().add("COMP1030");
        comboBox.getItems().addAll("COMP1008","MGMT5035","MGMT6035");
        /*
         * configuring RadioButton
         */
        radioLabel.setText("");
        favLangToggleGroup = new ToggleGroup(); //add buttons in here so only one can be selected.
        cPlusPlus.setToggleGroup(favLangToggleGroup);
        Java.setToggleGroup(favLangToggleGroup);
        phpRadioButton.setToggleGroup(favLangToggleGroup);
        cSharp.setToggleGroup(favLangToggleGroup);
        /*
         * configuring listview
         */
        listView.getItems().addAll("Golf Balls","Wedges","Irons","Tees","Driver","Putter");
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        /*
         * configuring spinner
         */
        SpinnerValueFactory<Integer> gradesValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(0,100,75);
        gradeSpinner.setValueFactory(gradesValueFactory);
        gradeSpinner.setEditable(true);
        gradeSpinner.getStyleClass().add(Spinner.STYLE_CLASS_SPLIT_ARROWS_VERTICAL);
        gradeLabel.setVisible(false);


    }



}
